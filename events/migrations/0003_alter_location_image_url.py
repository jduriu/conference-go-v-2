# Generated by Django 4.0.3 on 2022-09-29 22:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_location_image_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='image_url',
            field=models.URLField(null=True),
        ),
    ]
