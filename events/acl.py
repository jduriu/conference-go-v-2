import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_picture(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    search = {
        "query": f"{city} {state}",
        "per_page": 1,
    }
    response = requests.get(url, headers=headers, params=search)
    image_data = json.loads(response.content)

    try:
        return {"image_url": image_data["photos"][0]["src"]["original"]}
    except:
        return {"image_url": None}


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    code = "USA"
    search_geo = {
        "q": f"{city}, {state}, {code}",
        "appid": OPEN_WEATHER_API_KEY,
    }

    geo_response = requests.get(url, params=search_geo)
    geo_data = json.loads(geo_response.content)

    try:
        lat = geo_data[0]["lat"]
        lon = geo_data[0]["lon"]

        url = "https://api.openweathermap.org/data/2.5/weather"

        search_weather = {
            "lat": lat,
            "lon": lon,
            "units": "imperial",
            "appid": OPEN_WEATHER_API_KEY,
        }

        weather_response = requests.get(url, params=search_weather)
        weather_data = json.loads(weather_response.content)

        weather = {
            "temp": weather_data["main"]["temp"],
            "description": weather_data["weather"][0]["description"],
        }
        return weather
    except:
        return None
